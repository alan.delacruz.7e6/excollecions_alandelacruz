import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix la quantitat d'empleats")
    val quantempleats = scanner.nextInt()
    var infos = mutableListOf<MutableList<String>>()
    var info = mutableListOf<String>()
    var consulta = ""
    for (i in 1..quantempleats) {
        val dni = scanner.next()
        val nom = scanner.next()
        val cognom = scanner.next()
        val direccio = scanner.next()
        info.add(nom)
        info.add(cognom)
        info.add(dni)
        info.add(direccio)
        infos.add(info)
    }
    while (consulta != "end") {
        consulta = scanner.next()
        if (consulta == "end") {
            break
        }
        for (info in infos) {
            if (info[2] == consulta) {
                println("${info[0]} ${info[1]} - ${info[2]}, - ${info[3]}")
            } else {
                println("")
            }
        }


    }

}
