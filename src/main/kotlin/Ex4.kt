import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val frutas = mutableMapOf<String, Int>()
    var fruta = ""
    var contador = 0
    while (fruta != "end"){
        fruta = scanner.next()
        if (fruta == "end"){
            break
        }
        else if (frutas.containsKey(fruta)){
            contador++
        }
        else{
            frutas.put(fruta, 1)
        }
    }
    repeat(contador){
        println("MEEC!")
    }
}
