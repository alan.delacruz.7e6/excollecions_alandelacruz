import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val alumnes = mutableMapOf<String, Int>()
    var alumne = ""
    while (alumne != "end"){
        alumne = scanner.next()
        if (alumne == "end"){
            break
        }
        else if (alumne in alumnes){
            alumnes[alumne] = alumnes.getValue(alumne) +1
        }
        else{
            alumnes.put(alumne, 1)
        }
    }
    for (alumne in alumnes){
        println("${alumne}")
    }
}
