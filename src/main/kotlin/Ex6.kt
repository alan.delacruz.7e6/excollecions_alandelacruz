import java.util.*

interface GymControlReader{
    fun nextId(): String
}

class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}



fun main(args: Array<String>) {

    var idllegits = mutableListOf<String>()
    for (i in 1..8){
        val lector = GymControlManualReader()
        val id = lector.nextId()
        if (idllegits.contains(id)){
            println("$id sortida")
            idllegits.remove(id)
        }
        else{
            idllegits.add(id)
            println("$id entrada")
        }
    }

}

