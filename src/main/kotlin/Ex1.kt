import java.util.Scanner


fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix el número de cartells")
    val numcartells = scanner.nextInt()
    var cartells = mutableMapOf<String, String>()
    for (i in 1..numcartells){
        println("Introdueix els metres y el text del cartell")
        val metres = scanner.next()
        val textcartell = scanner.next()
        cartells.put(metres,textcartell )
    }
    println("Introdueix el numero de consultes")
    val numconsultes = scanner.nextInt()
    for (j in 1..numconsultes){
        println("Introdueix la primera consulta ficant els metres")
        val metreconsulta = scanner.next()
        if (cartells.containsKey(metreconsulta)){
            println(cartells[metreconsulta])
        }
        else{
            println("No hi ha cartell")
        }
    }
}